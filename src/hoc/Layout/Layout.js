import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from '../../hoc/Aux/Aux';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

import classes from './layout.module.scss';

class Layout extends Component {

  state = {
    showSideDrawer: false
  }

  sideDrawerClosedHandler = () => {
    this.setState({ showSideDrawer: false });
  }

  sideDrawerOpenedHandler = () => {
    this.setState((prevState) => { 
      return {showSideDrawer: !this.state.showSideDrawer }
    });
  }

  render () {
    return (
      <Aux>
        <Toolbar
          opened={this.sideDrawerOpenedHandler}
          isAuthenticated={this.props.isAuthenticated} />
        <SideDrawer
          show={this.state.showSideDrawer}
          closed={this.sideDrawerClosedHandler}
          isAuthenticated={this.props.isAuthenticated}
        />
        <main className={classes.content}>
          { this.props.children }
        </main>
      </Aux>
    );
  } 
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
}

export default connect(mapStateToProps)(Layout);
import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://react-burger-project-278307.firebaseio.com/'
});

/* instance.interceptors.request.use(request => {
  console.log('request');
  console.log(request);
  // Edit request config (ex: add headers)
  return request; //have to return the request if not it will be blocked
}, error => { // this error is link to sending the request (ex: no internet)
  console.log(error);
  return Promise.reject(error);
});

instance.interceptors.request.use(response => {
  console.log('response');
  console.log(response);
  // Edit response config
  return response; 
}, error => { 
  console.log(error);
  return Promise.reject(error);
}); */

export default instance;
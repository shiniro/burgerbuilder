import React, { Component } from 'react';
import axios from '../../../axios-orders';
import { connect } from 'react-redux';

import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../../store/actions'
import { updateObject, checkValidity } from '../../../shared/utility';

import classes from './ContactData.module.scss';

class ContactData extends Component {

  state = {
    orderForm: {
      name: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Name'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      street: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Street'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      zipcode: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Zipcode'
        },
        value: '',
        validation: {
          required: true,
          maxLength: 5,
          minLength: 5
        },
        valid: false,
        touched: false
      },
      country: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Country'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Your Email'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      deliveryMethod: {
        elementType: 'select',
        elementConfig: {
          options: [
            {
              value: 'fastert',
              displayValue: 'Fastest'
            },
            {
              value: 'cheapest',
              displayValue: 'Chepest'
            }
          ]
        },
        value: 'fastest',
        validation: {},
        valid: true,
      }
    },
    // loading: false,
    formIsValid: false
  }

  orderHandler = (event) => {
    event.preventDefault();
    // this.setState({ loading: true });

    const formData = {};
    for(let formElementIdentifier  in this.state.orderForm) {
      formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier];
    }

    const order = {
      ingredients: this.props.ings,
      price: this.props.price, // should calculate the price on the server to make sure the user is not manipulating the price
      orderData: formData,
      userId: this.props.userId
    };

    this.props.onOrderBurger(order, this.props.token);

    // axios.post('/orders.json', order) // .json because of firebase
    //   .then(response => {
    //     console.log(response);
    //     this.setState({ loading: false });
    //     this.props.history.push('/');
    //   })
    //   .catch(error => {
    //     console.log(error);
    //     this.setState({ loading: false });
    //   });
  }

  inputChangedHandler = (event, inputIdentifier) => {
    
    const updatedFormElement = updateObject(this.state.orderForm[inputIdentifier], {
      value: event.target.value,
      valid: checkValidity(event.target.value, this.state.orderForm[inputIdentifier].validation),
      touched: true
    });
    const updatedOrderForm = updateObject(this.state.orderForm, {
      [inputIdentifier]: updatedFormElement
    });

    let formIsValid = true;
    for(let inputIdentifiers in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifiers].valid && formIsValid;
    }

    this.setState({ orderForm: updatedOrderForm, formIsValid: formIsValid });
  }

  render() {

    const formElelemtArray = [];

    for (let key in this.state.orderForm) {
      formElelemtArray.push({
        id: key,
        config: this.state.orderForm[key]
      })
    }

    let form = (
      <form onSubmit={this.orderHandler}>
        {
          formElelemtArray.map(element => (
            <Input
              key={element.id}
              elementType={element.config.elementType}
              elementConfig={element.config.elementConfig}
              value={element.config.value}
              invalid={!element.config.valid}
              shouldValidate={element.config.validation}
              touched={element.config.touched}
              changed={(event) => this.inputChangedHandler(event, element.id)}
              />
          ))
        }
        <Button btnTypes="success" disabled={!this.state.formIsValid}>Order</Button>
      </form>
    );
    if(this.props.loading) {
      form = <Spinner />
    }

    return (
      <div className={classes.contactData}>
        <h2>Enter your contact Data</h2>
        {form}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    ings: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    loading: state.order.loading,
    token: state.auth.token,
    userId: state.auth.userId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onOrderBurger: (orderData, token) => dispatch(actions.purchaseBurger(orderData, token))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData, axios));
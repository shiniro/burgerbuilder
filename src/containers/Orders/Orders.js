import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from '../../axios-orders';
import * as actions from '../../store/actions';

import Order from '../../components/Order/Order';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../components/UI/Spinner/Spinner';

import classes from './orders.module.scss';

class Orders extends Component {

  componentDidMount() {
    this.props.onFetchOrders(this.props.token, this.props.userId);
    // axios.get('/orders.json')
    //   .then(response => {
    //     this.setState({ loading: false });
    //     const fetchedOrders = [];
    //     for (let key in response.data) {
    //       fetchedOrders.push({
    //         ...response.data[key],
    //         id: key
    //       });
    //     }
    //     this.setState({ orders: fetchedOrders });
    //   })
    //   .catch(error => {
    //     this.setState({ loading: false });
    //   });
  }

  render() {

    let orders = <Spinner />
    if(!this.props.loading) {
      orders = this.props.orders.map(order => (
          <Order
            key={order.id}
            ingredients={order.ingredients}
            price={order.price}
          />
        ));
      }


    return (
      <div className={classes.orders}>
        {orders}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    orders: state.order.orders,
    loading: state.order.loading,
    token: state.auth.token,
    userId: state.auth.userId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchOrders: (token, userId) => dispatch(actions.fetchOrders(token, userId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Orders, axios));
import React from 'react';

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';

import classes from './CheckoutSummary.module.scss';

const checkoutSummary = (props) => {
  return (
    <div className={classes.checkoutSummary}>
      <h1>We hope it tastes well !</h1>
      <div className={classes.burger}>
        <Burger ingredients={props.ingredients} />
      </div>
      <Button
        btnTypes="danger"
        clicked={props.onCheckoutCancelled}>Cancel</Button>
      <Button
        btnTypes="success"
        clicked={props.onCheckoutContinued}>Continue</Button>
    </div>
  );
}

export default checkoutSummary;
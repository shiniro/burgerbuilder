import React from 'react';

import classes from './order.module.scss';

const order = (props) => {

  const ingredients = [];

  for(let ingredientName in props.ingredients) {
    ingredients.push({
      name: ingredientName,
      amount: props.ingredients[ingredientName]
    })
  }

  const ingredientOutput = ingredients.map(ingredient => {
    return <span key={ingredient.name}>{ingredient.name} ({ingredient.amount}) </span>
  })

  return (
    <div className={classes.order}>
      <p>Ingredients: {ingredientOutput} </p>
      <p>Prince: <b>USD {Number.parseFloat(props.price.toFixed(2))}</b></p>
    </div>
  )
};

export default order;
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Aux from '../../../hoc/Aux/Aux';
import Button from '../../UI/Button/Button';

import classes from './orderSummary.module.scss';

class OrderSummary extends Component {

  render () {

    const ingredientSummary = Object.keys(this.props.ingredients)
      .map(igKey => {
        return <li key={igKey} ><span>{igKey}</span>: {this.props.ingredients[igKey]}</li>
      });

    return (
      <Aux>
        <h3>Your Order</h3>
        <p>A delicious burger with the following ingredients:</p>
        <ul className={classes.orderSummary}>
          {ingredientSummary}
        </ul>
        <p><b>Total price: {this.props.price.toFixed(2)} </b></p>
        <p>Continue to checkout ?</p>
        <Button btnTypes="danger" clicked={this.props.purchaseCancelled}>Cancel</Button>
        <Button btnTypes="success" clicked={this.props.purchaseContinued}>Continue</Button>
      </Aux>
    );
  }
};

OrderSummary.propTypes = {
  purchaseCancelled: PropTypes.func.isRequired,
  purchaseContinued: PropTypes.func.isRequired
}

export default OrderSummary;
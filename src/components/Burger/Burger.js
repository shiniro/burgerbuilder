import React from 'react';
import PropTypes from 'prop-types';

import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

import classes from './burger.module.scss';

const burger = (props) => {

  let transformedIngredients = Object.keys(props.ingredients) // only array of keys : cheese, salad, meat, bacon
    .map(igKey => {
      return [...Array(props.ingredients[igKey])].map((_, i) => {
        return <BurgerIngredient key={igKey + i} type={igKey} />
      });
    }) // here : [[salad], [bacon], [cheese, cheese], [meat, meat]]
    .reduce((arr, el) => { //arr: previous array, []: initial value
      return arr.concat(el)
    }, []); // here : [[salad, bacon, cheese, cheese, meat, meat]

  if(transformedIngredients.length === 0)
    transformedIngredients = <p>Please start adding ingredients !</p>

  return (
    <div className={classes.burger}>
      <BurgerIngredient type="bread-top" />
      { transformedIngredients }
      <BurgerIngredient type="bread-bottom" />
    </div>
  );
};

burger.propTypes = {
  ingredients: PropTypes.object.isRequired
}

export default burger;
import React from 'react';
import PropTypes from 'prop-types';

import BuildControl from './BuildControl/BuildControl';

import classes from './buildControls.module.scss';

const controls = [
  {
    label: 'Salad',
    type: 'salad'
  },
  {
    label: 'Bacon',
    type: 'bacon'
  },
  {
    label: 'Cheese',
    type: 'cheese'
  },
  {
    label: 'meat',
    type: 'meat'
  }
];

const buildControls = (props) => (
  <div className={classes.buildControls}>
    <p>Current price: <strong>{props.price.toFixed(2)}</strong></p>
    {
      controls.map((ctrl) => (
        <BuildControl
          key={ctrl.label}
          label={ctrl.label} 
          added={() => props.ingredientAdded(ctrl.type)}
          removed={() => props.ingredientRemoved(ctrl.type)}
          disabled={props.disabled[ctrl.type]}
        />
      ))
    }
    <button className={classes.orderButton} disabled={ !props.purchasable } onClick={props.ordering}>{ props.isAuthenticated ? 'Order now' : 'Sign up to order' }</button>
  </div>
);

buildControls.propTypes = {
  purchasable: PropTypes.bool.isRequired,
  ordering: PropTypes.func.isRequired
}

export default buildControls;
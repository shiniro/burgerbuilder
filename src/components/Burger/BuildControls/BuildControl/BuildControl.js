import React from 'react';
import PropTypes from 'prop-types';

import classes from './buildControl.module.scss';

const buildControl = (props) => (
  <div className={classes.buildControl}>
    <div className={classes.label}>{ props.label }</div>
    <button className={classes.less} onClick={props.removed} disabled={props.disabled}>Less</button>
    <button className={classes.more} onClick={props.added}>More</button>
  </div>
);

buildControl.propTypes = {
  label: PropTypes.string.isRequired,
  removed: PropTypes.func,
  disabled: PropTypes.bool.isRequired,
  added: PropTypes.func,
}

export default buildControl;
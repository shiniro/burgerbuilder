import React from 'react';
import PropTypes from 'prop-types';

import classes from './logo.module.scss';
import Logo from '../../assets/img/burger-logo.png';

const logo = (props) => (
  <div className={classes.logo} style={{ height: props.height }}>
    <img src={Logo} alt="logo" />
  </div>
);

logo.propTypes = {
  height: PropTypes.string.isRequired
}

export default logo;
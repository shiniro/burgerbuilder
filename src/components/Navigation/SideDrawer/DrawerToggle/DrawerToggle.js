import React from 'react';
import PropTypes from 'prop-types';

import classes from './drawerToogle.module.scss';

const drawerToggle = (props) => (
  <div className={classes.drawerToggle} onClick={props.clicked}>
    <span>Menu</span>
  </div>
);

drawerToggle.propTypes = {
  clicked: PropTypes.func.isRequired
}

export default drawerToggle;
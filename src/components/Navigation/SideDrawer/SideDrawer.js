import React from 'react';
import PropTypes from 'prop-types';

import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Aux from '.././../../hoc/Aux/Aux';
import Backdrop from '../../UI/Backdrop/Backdrop';

import classes from './sideDrawer.module.scss';

const sideDrawer = (props) => {

  let attachedClasses = [classes.sideDrawer, classes.close];

  if (props.show) {
    attachedClasses = [classes.sideDrawer, classes.open]
  }

  return (
    <Aux>
      <div className={attachedClasses.join(' ')} onClick={props.closed}>
        <Logo height="11%" />
        <nav>
          <NavigationItems isAuthenticated={props.isAuthenticated} />
        </nav>
      </div>
      <Backdrop show={props.show} clicked={props.closed} />
    </Aux>
  );
};

sideDrawer.propTypes = {
  show: PropTypes.bool,
  closed: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool
}

export default sideDrawer;
import React from 'react';
import PropTypes from 'prop-types';

import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';

import classes from './toolbar.module.scss';

const toolbar = (props) => (
  <header className={classes.toolbar}>
    <DrawerToggle clicked={props.opened} />
    <Logo height="80%" />
    <nav className={ classes.desktopOnly }>
      <NavigationItems isAuthenticated={props.isAuthenticated} />
    </nav>
  </header>
);

toolbar.propTypes = {
  opened: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool
}

export default toolbar;
import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import classes from './navigationItem.module.scss';

const navigationItem = (props) => (
  <li className={classes.navigationItem}>
    <NavLink
      to={props.link}
      exact={props.exact}
      activeClassName={classes.active}
    >{ props.children }</NavLink>
  </li>
);

navigationItem.propTypes = {
  link: PropTypes.string.isRequired,
}

export default navigationItem;
import React from 'react';
import PropTypes from 'prop-types';

import NavigationItem from './NavigationItem/NavigationItem';

import classes from './navigationItems.module.scss';

const navigationItems = (props) => (
  <ul className={classes.navigationItems}>
    <NavigationItem link="/" exact>Burger Builder</NavigationItem>
    { props.isAuthenticated ? <NavigationItem link="/orders">Orders</NavigationItem> : null }
    { props.isAuthenticated ?
      <NavigationItem link="/logout">Logout</NavigationItem>
      : <NavigationItem link="/auth">Authenticate</NavigationItem>
    }
  </ul>
);

navigationItems.propTypes = {
  isAuthenticated: PropTypes.bool
}


export default navigationItems;
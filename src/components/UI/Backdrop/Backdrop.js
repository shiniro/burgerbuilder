import React from 'react';
import PropTypes from 'prop-types';

import classes from './backdrop.module.scss';

const backdrop = (props) => (
  props.show ? 
    <div className={classes.backdrop} onClick={props.clicked}></div>
    : null
);

backdrop.propTypes = {
  show: PropTypes.bool,
  clicked: PropTypes.func.isRequired
}

export default backdrop;